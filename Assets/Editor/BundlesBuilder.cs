﻿using UnityEditor;
using System.IO;

public class BundlesBulder
{
    [MenuItem( "Assets/Build bundles" )]
    static void BuildAllAssetBundles()
    {
        string assetBundleDirectory = "Assets/BundleData";
        if( !Directory.Exists( assetBundleDirectory ) )
        {
            Directory.CreateDirectory( assetBundleDirectory );
        }
        BuildPipeline.BuildAssetBundles( assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows );
    }

}
