﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GameController : MonoBehaviour
{
    public SettingsScriptable Settings;
    public TextsController Texts;
    public GameObject QuizLetter;
    public GameObject Letter;
    public Text NumPointsText;
    public Text NumTriesText;
    public GameObject StartMenu;
    public GameObject LosePanel;
    public GameObject WinPanel;

    public Transform MainWord;
    public Transform Letters;

    private int CurrentTries = 0;
    private int CurrentPoints = 0;

    Dictionary<char, List<GameObject>> quiz_letters;

    void OnEnable()
    {
        StartCoroutine( StartGame() );
    }

    IEnumerator StartGame()
    {
        while( !Texts.IsReady )
            yield return null;
        Texts.PrepareForNewGame();
        NewTurn();
        CurrentTries = 0;
        CurrentPoints = 0;
        NumTriesText.text = Settings.NumTries.ToString();
        NumPointsText.text = "0";
        WinPanel.SetActive( false );
        LosePanel.SetActive( false );
    }

    private void ClearTransforms()
    {
        foreach( Transform t in MainWord )
        {
            Destroy( t.gameObject );
        }
        foreach( Transform t in Letters )
        {
            Destroy( t.gameObject );
        }
    }

    private void NewTurn()
    {
        ClearTransforms();
        quiz_letters = new Dictionary<char, List<GameObject>>();
        string word = Texts.GetRandomWord();

        if( word == "" )
            WinPanel.SetActive( true );

        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToLower();
        List<char> letters = new List<char>();
        foreach( char c in word )
        {
            GameObject quiz_letter = Instantiate( QuizLetter );
            quiz_letter.transform.SetParent( MainWord );
            if( chars.Contains( c ) )
                chars = chars.Replace( c.ToString(), "" );
            if( !letters.Contains( c ) )
                letters.Add( c );
            quiz_letter.transform.Find( "Text" ).GetComponent<Text>().text = c.ToString().ToUpper();
            if( quiz_letters.ContainsKey( c ) )
                quiz_letters[c].Add( quiz_letter );
            else
                quiz_letters.Add( c, new List<GameObject>() { quiz_letter } );
        }

        int i = word.Length / 2;

        while( i != 0 )
        {
            char c = chars[Random.Range( 0, chars.Length )];
            letters.Add( c );
            chars = chars.Replace( c.ToString(), "" );
            i--;
        }

        letters = letters.OrderBy( item => Random.Range( 0, letters.Count ) ).ToList();

        foreach( char l in letters )
        {
            GameObject letter = Instantiate( Letter );
            letter.transform.SetParent( Letters );
            letter.transform.Find( "Text" ).GetComponent<Text>().text = l.ToString().ToUpper();
            char local_l = l;
            letter.GetComponent<Button>().onClick.AddListener( delegate
            {
                TryLetter( l );
                Destroy( letter );
            } );
        }
    }

    private void TryLetter( char c )
    {
        if( quiz_letters.ContainsKey( c ) )
        {
            foreach( GameObject go in quiz_letters[c] )
            {
                go.GetComponent<Image>().enabled = false;
            }
            if( quiz_letters.Values.ToList().Find( q => q.Find( go => go.GetComponent<Image>().enabled ) != null ) == null )
            {
                NewTurn();
                CurrentPoints += Settings.NumTries - CurrentTries;
                NumPointsText.text = CurrentPoints.ToString();
                return;
            }
        }
        else
        {
            CurrentTries++;
            if( Settings.NumTries - CurrentTries < 0 )
            {
                LosePanel.SetActive( true );
                return;
            }
            NumTriesText.text = ( Settings.NumTries - CurrentTries ).ToString();
        }
    }
}
