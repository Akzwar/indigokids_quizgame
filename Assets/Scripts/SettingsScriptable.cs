﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "QuizSettings", menuName = "QuizSettings", order = 1)]
public class SettingsScriptable : ScriptableObject
{
    public int MinWordLength = 4;
    public int NumTries = 5;
}