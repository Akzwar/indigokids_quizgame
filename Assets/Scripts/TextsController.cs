﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using UnityEngine.UI;

public class TextsController : MonoBehaviour
{
    public SettingsScriptable Settings;
    public string BundleURL;
    public Toggle UseMostFrequentToggle;

    Dictionary<string, int> words;//word, rating

    private Dictionary<string, int> current_words;

    public bool IsReady
    {
        get;
        private set;
    }

    public bool UseMostFrequent
    {
        get;
        private set;
    }

    public void SetUseMostFrequent( bool value )
    {
        UseMostFrequent = value;
    }

    IEnumerator Start()
    {
        words = new Dictionary<string, int>();
        current_words = new Dictionary<string, int>();

        UnityWebRequest request = UnityWebRequest.GetAssetBundle( BundleURL, 0 );
        UnityWebRequestAsyncOperation request_result = request.SendWebRequest();
        while( !request_result.isDone )
            yield return null;
        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent( request );

        string[] text_names = bundle.GetAllAssetNames();
        foreach( string text_name in text_names )
        {
            TextAsset asset = bundle.LoadAsset<TextAsset>( text_name );
            if( asset != null )
                ApplyQuizWords( asset, Settings.MinWordLength );
        }
        IsReady = true;
        UseMostFrequentToggle.isOn = UseMostFrequent;
    }

    public void PrepareForNewGame()
    {
        current_words = words.ToDictionary(pair => pair.Key, pair => pair.Value);
    }

    public string GetRandomWord()
    {
        if( current_words.Count == 0 )
            return "";
        if( UseMostFrequent && current_words.Values.ToList().FindIndex( i => i > 1 ) != -1 )
        {
            List<string> frequent_words = current_words.Where( q => q.Value > 1 ).Select( q => q.Key ).ToList();
            int index = Random.Range( 0, frequent_words.Count - 1 );
            string word = frequent_words[index];
            current_words.Remove( word );
            return word;
        }
        else
        {
            int index = Random.Range( 0, current_words.Count - 1 );
            string word = current_words.Keys.ToList()[index];
            current_words.Remove( word );
            return word;
        }
    }

    private void ApplyQuizWords( TextAsset asset, int MinWordLength )
    {
        string text = asset.text.ToLower();
        text = new string( text.Where( c => char.IsLetter( c ) || char.IsWhiteSpace( c ) || c == '-' ).ToArray() );
        text = new Regex( @"\s+" ).Replace(text, " ");
        text = text.Replace( "--", " " );
        text = text.Replace( "-", " " );
        List<string> all_words = text.Split( ' ' ).ToList().Where( s => s != " " && s.Length >= MinWordLength ).ToList();
        foreach( string word in all_words )
        {
            if( words.ContainsKey( word ) )
                words[word]++;
            else
                words.Add( word, 1 );
        }
    }
}
